#include <stdio.h>
#include "mysql.c"
#include "pkg_m.c"
#include "color.h"

void playgame()
{
    printf( "Play game called\n" );
}
void loadgame()
{
    printf( "Load game called" );
}
void playmultiplayer()
{
    printf( "Play multiplayer game called" );
}
	
int main()
{
	int input;
	int i;

    printf( "M2Tec Server Installer by .Kori\n" );
    printf( "Informations: https://www.m2tec.net/ \n" );
    printf( "1 - PKG Menue\n" );
    printf( "2 - Mysql Menue\n" );
    printf( "Selection: " );
    scanf( "%d", &input );
    switch ( input ) {
        case 1:
			pkg_m();
         
            break;
        case 2:          
            mysql_m();
            break;
        case 3:         
            playmultiplayer();
            break;
        case 4:        
            printf( "Thanks for playing!\n" );
            break;
        default:            
            printf(ANSI_COLOR_RED "Bad input, quitting!\n" ANSI_COLOR_RESET);
            break;
    }
    getchar();

}
