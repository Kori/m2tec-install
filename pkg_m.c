#include <stdio.h>
#include "color.h"


void pkg_m() {
	int i;
	system("clear");
	printf("PKG Menue:\n");
	printf("1 - Check for PKG\n");
	printf("2 - Install PKG\n");
	printf("3 - Back\n"); 
	scanf("%d" , &i);
	switch(i) {
		case 1:
			printf("\n");
			system("pkg -v");
			printf( ANSI_COLOR_GREEN "\nWhen you see a version then dont install pkg!!\n" ANSI_COLOR_RESET);
			break;
		case 2:
			system("cd /usr/ports/ports-mgmt/pkg && make && make install clean");
			break;
		case 3:
			system("clear");
			main();
		default:
			system("clear");
			printf(ANSI_COLOR_RED "Bad input!\n" ANSI_COLOR_RESET);
			main();
			break;
	}
}

